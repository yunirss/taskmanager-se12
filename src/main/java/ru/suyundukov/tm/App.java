package ru.suyundukov.tm;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.suyundukov.tm.utils.AppConfig;


/**
 * Основной класс
 */
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }
}