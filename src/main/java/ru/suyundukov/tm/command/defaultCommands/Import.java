package ru.suyundukov.tm.command.defaultCommands;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.Data;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
@Component
public class Import extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[IMPORT]");
        System.out.println("[SELECT AN OPTION. ENTER NUMBER]");
        System.out.println("[1. from .json FILE with fasterXML]");
        System.out.println("[2. from .xml FILE with fasterXML]");
        System.out.println("[3. from .bin FILE]");
        String answer = ConsoleHelper.readString().trim();
        switch (answer) {
            case "1":
                fromJSON();
                break;
            case "2":
                fromXML();
                break;
            case "3":
                fromBIN();
                break;
        }
    }

    private void fromJSON() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        Data data = objectMapper.readValue(new File("data.json"), Data.class);
        projectService.load(data.getProjects());
        taskService.load(data.getTasks());
        userService.load(data.getUsers());
        System.out.println("[OK]");
    }

    private void fromXML() throws Exception {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.registerModule(new JavaTimeModule());
        Data data = xmlMapper.readValue(new File("data.xml"), Data.class);
        projectService.load(data.getProjects());
        taskService.load(data.getTasks());
        userService.load(data.getUsers());
        System.out.println("[OK]");
    }

    private void fromBIN() throws Exception {
        FileInputStream fileInputStream = new FileInputStream("data.bin");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        loadProjects(objectInputStream.readObject());
        loadTasks(objectInputStream.readObject());
        loadUsers(objectInputStream.readObject());
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

    private void loadProjects(final Object value) throws Exception {
        if (!(value instanceof Project[])) return;
        final Project[] projects = (Project[]) value;
        projectService.load(projects);
    }

    private void loadTasks(final Object value) throws Exception {
        if (!(value instanceof Task[])) return;
        final Task[] tasks = (Task[]) value;
        taskService.load(tasks);
    }

    private void loadUsers(final Object value) throws Exception {
        if (!(value instanceof User[])) return;
        final User[] users = (User[]) value;
        userService.load(users);
    }

    @Override
    public String getName() {
        return "import";
    }

    @Override
    public String getDescription() {
        return "Import to file.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
