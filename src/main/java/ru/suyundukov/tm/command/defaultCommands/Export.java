package ru.suyundukov.tm.command.defaultCommands;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.Data;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
@Component
public class Export extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[EXPORT]");
        System.out.println("[SELECT AN OPTION. ENTER NUMBER]");
        System.out.println("[1. to .json FILE with fasterXML]");
        System.out.println("[2. to .xml FILE with fasterXML]");
        System.out.println("[3. to .bin FILE]");
        String answer = ConsoleHelper.readString().trim();
        switch (answer) {
            case "1":
                toJSON();
                break;
            case "2":
                toXML();
                break;
            case "3":
                toBIN();
                break;
        }
    }

    private void toJSON() throws Exception {
        Data data = new Data();
        data.setUsers(userService.findAll());
        data.setTasks(taskService.findAll());
        data.setProjects(projectService.findAll());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        final String json = objectWriter.writeValueAsString(data);
        final byte[] allData = json.getBytes(StandardCharsets.UTF_8);
        final File file = new File("data.json");
        Files.write(file.toPath(), allData);
        System.out.println("[OK]");
    }

    private void toXML() throws Exception {
        Data data = new Data();
        data.setUsers(userService.findAll());
        data.setTasks(taskService.findAll());
        data.setProjects(projectService.findAll());
        ObjectMapper objectMapper = new XmlMapper();
        objectMapper.registerModule(new JavaTimeModule());
        final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        final String json = objectWriter.writeValueAsString(data);
        final byte[] dataByte = json.getBytes(StandardCharsets.UTF_8);
        final File file = new File("data.xml");
        Files.write(file.toPath(), dataByte);
        System.out.println("[OK]");
    }

    private void toBIN() throws Exception {
        Data data = new Data();
        data.setUsers(userService.findAll());
        data.setTasks(taskService.findAll());
        data.setProjects(projectService.findAll());
        final Project[] projects = projectService.findAll().toArray(new Project[]{});
        final Task[] tasks = taskService.findAll().toArray(new Task[]{});
        final User[] users = userService.findAll().toArray(new User[]{});
        final File file = new File("data.bin");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(projects);
        objectOutputStream.writeObject(tasks);
        objectOutputStream.writeObject(users);
        objectOutputStream.close();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "export";
    }

    @Override
    public String getDescription() {
        return "Export to file.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
