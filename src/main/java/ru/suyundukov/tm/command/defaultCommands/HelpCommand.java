package ru.suyundukov.tm.command.defaultCommands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * Класс, для работы с сущностью проекта, содержащий дефолтные команды
 */
@Component
public class HelpCommand extends AbstractCommand {
    @Autowired
    private List<AbstractCommand> commands;
    @Override
    public void execute() throws Exception {
        List<String> listOfCommands = new ArrayList<>();
        listOfCommands.add(this.toString());
        if (userService.getUserOnline() == null) {
            for (AbstractCommand command : commands) {
                if (!command.isNeedAuthorization()) {
                    listOfCommands.add(command.getName() + " : " + command.getDescription());
                }
            }
        } else {
            for (AbstractCommand command : commands) {
                listOfCommands.add(command.getName() + " : " + command.getDescription());
            }
        }
        Collections.sort(listOfCommands);
        for (String s : listOfCommands) {
            System.out.println(s);
        }
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return false;
    }
}
