package ru.suyundukov.tm.command.defaultCommands;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
@Component
public class ExitCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit the Task manager";
    }

    @Override
    public boolean isNeedAuthorization() {
        return false;
    }
}
