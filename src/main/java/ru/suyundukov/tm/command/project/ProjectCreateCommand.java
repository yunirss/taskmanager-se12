package ru.suyundukov.tm.command.project;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.enums.Status;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.time.LocalDateTime;

@Component
public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATED]");
        System.out.println("[ENTER NAME:]");
        String name = ConsoleHelper.readString();
        System.out.println("[ENTER DESCRIPTION:]");
        String description = ConsoleHelper.readString();
        projectService.save(new Project(null, name, description, LocalDateTime.now(),
                userService.getUserOnline().getId(), Status.OPEN));
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
