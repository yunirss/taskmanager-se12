package ru.suyundukov.tm.command.project;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProjectListCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        List<Project> projectList = projectService.findAll();
        final User userOnline = userService.getUserOnline();
        if (!userOnline.getRoleType().equals(RoleType.ADMIN)) {
            List<Project> projectValid = projectList.stream()
                    .filter(x -> x.getUserId() == userOnline.getId())
                    .sorted(Comparator.comparing(Project::getName))
                    .collect(Collectors.toList());
            for (int i = 0; i < projectValid.size(); i++) {
                System.out.println((i + 1) + ". " + projectValid.get(i).getName());
            }
        } else {
            projectList.sort(Comparator.comparing(Project::getName));
            for (int i = 0; i < projectList.size(); i++) {
                System.out.println((i + 1) + ". " + projectList.get(i).getName());
            }
        }
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
