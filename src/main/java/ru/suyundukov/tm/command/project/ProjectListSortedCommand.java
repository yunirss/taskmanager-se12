package ru.suyundukov.tm.command.project;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;
import ru.suyundukov.tm.utils.DateHelper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProjectListSortedCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[SORTING]");
        System.out.println("[WHAT TO DO? ENTER NUMBER]");
        System.out.println("1. SORT BY STATUS");
        System.out.println("2. SORT BY CREATE DATE");
        System.out.println("3. FIND BY UNQUALIFIED NAME");
        int answer = Integer.parseInt(ConsoleHelper.readString().trim());
        List<Project> projectList = projectService.findAll();
        final User userOnline = userService.getUserOnline();
        if (answer == 1) {
            System.out.println("[PROJECT SORTED LIST BY STATUS]");
            if (!userOnline.getRoleType().equals(RoleType.ADMIN)) {
                List<Project> projectValid = projectList.stream()
                        .filter(x -> x.getUserId() == userOnline.getId())
                        .sorted(Comparator.comparing(Project::getStatus))
                        .collect(Collectors.toList());
                for (int i = 0; i < projectValid.size(); i++) {
                    System.out.println((i + 1) + ". " + projectValid.get(i).getName() + " - " + projectValid.get(i).getStatus());
                }
            } else {
                projectList.sort(Comparator.comparing(Project::getStatus));
                for (int i = 0; i < projectList.size(); i++) {
                    System.out.println((i + 1) + ". " + projectList.get(i).getName() + " - " + projectList.get(i).getStatus());
                }
            }
        } else if (answer == 2) {
            System.out.println("[PROJECT SORTED LIST BY CREATE DATE]");
            if (!userOnline.getRoleType().equals(RoleType.ADMIN)) {
                List<Project> projectValid = projectList.stream()
                        .filter(x -> x.getUserId() == userOnline.getId())
                        .sorted(Comparator.comparing(Project::getCreateDate))
                        .collect(Collectors.toList());
                for (int i = 0; i < projectValid.size(); i++) {
                    System.out.println((i + 1) + ". " + projectValid.get(i).getName() + " - " + DateHelper.dateFormat(projectValid.get(i).getCreateDate()));
                }
            } else {
                projectList.sort(Comparator.comparing(Project::getCreateDate));
                for (int i = 0; i < projectList.size(); i++) {
                    System.out.println((i + 1) + ". " + projectList.get(i).getName() + " - " + DateHelper.dateFormat(projectList.get(i).getCreateDate()));
                }
            }
        } else if (answer == 3) {
            System.out.println("[FIND BY UNQUALIFIED NAME]");
            System.out.println("[ENTER NAME]");
            String name = ConsoleHelper.readString().trim();
            if (name.length() >= 3) {
                List<Project> projectTmp = new ArrayList<>();
                List<Project> projects = projectList.stream()
                        .filter(x -> userOnline.getId() == x.getUserId())
                        .sorted(Comparator.comparing(Project::getName))
                        .collect(Collectors.toList());
                for (int i = 0; i < projects.size(); i++) {
                    if (projects.get(i).getName().startsWith(name)) {
                        System.out.println((i + 1) + ". " + projects.get(i).getName());
                        projectTmp.add(projects.get(i));
                    }
                }
                if (projectTmp.isEmpty()) {
                    System.out.println("[PROJECTS WITH THIS NAME NOT FOUND]");
                }
            } else {
                System.out.println("[ENTER AT LEAST 3 LETTERS]");
            }
        } else {
            System.out.println("[TRY AGAIN]");
        }
    }

    @Override
    public String getName() {
        return "project-list-sorted";
    }

    @Override
    public String getDescription() {
        return "Show all sorted projects";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
