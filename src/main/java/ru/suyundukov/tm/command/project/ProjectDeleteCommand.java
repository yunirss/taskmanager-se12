package ru.suyundukov.tm.command.project;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;

@Component
public class ProjectDeleteCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT-DELETE] :");
        System.out.println("[ENTER PROJECT NAME] :");
        String projectName = ConsoleHelper.readString();
        if (projectService.findByName(projectName) != null) {
            final Project project = projectService.findByName(projectName);
            final User userOnline = userService.getUserOnline();
            if (project.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
                System.out.println("[DELETE PROJECT " + "<<" + projectName + ">>" + " ? YES or NO]");
                String answer = ConsoleHelper.readString();
                if (answer.equalsIgnoreCase("yes")) {
                    taskService.deleteTasksByProjectId(project.getId());
                    projectService.deleteByName(projectName);
                    System.out.println("[SELECTED PROJECT DELETED]");
                } else {
                    System.out.println("[NOT DELETED]");
                }
                return;
            }
            System.out.println("[ACCESS DENIED]");
        } else {
            System.out.println("[NO SUCH PROJECT EXIST]");
        }
    }

    @Override
    public String getName() {
        return "project-delete";
    }

    @Override
    public String getDescription() {
        return "Delete selected project.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
