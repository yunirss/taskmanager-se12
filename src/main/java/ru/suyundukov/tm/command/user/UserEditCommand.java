package ru.suyundukov.tm.command.user;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.utils.ConsoleHelper;
import ru.suyundukov.tm.utils.DateHelper;
@Component
public class UserEditCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[USER EDIT]");
        if (userService.getUserOnline() == null) {
            System.out.println("THERE IS NO USERS TO EDIT, PLEASE SIGN IN");
            return;
        }
        System.out.println("User Id: " + userService.getUserOnline().getId());
        System.out.println("User Login: " + userService.getUserOnline().getLogin());
        System.out.println("User Role-type: " + userService.getUserOnline().getRoleType().getName());
        System.out.println("User name: " + userService.getUserOnline().getName());
        System.out.println("User create date: " + DateHelper.dateFormat(userService.getUserOnline().getCreateDate()));
        System.out.println("[ENTER: [NAME] or [PASSWORD] to EDIT");
        String field = ConsoleHelper.readString().trim();
        if (field.equalsIgnoreCase("name")) {
            System.out.println("[ENTER NEW NAME]");
            String name = ConsoleHelper.readString();
            userService.getUserOnline().setName(name);
            System.out.println("[NAME HAS BEEN CHANGED]");
        } else if (field.equalsIgnoreCase("password")) {
            System.out.println("[ENTER NEW PASSWORD]");
            String password = ConsoleHelper.readString();
            String newPassword = ConsoleHelper.md5Password(password);
            userService.getUserOnline().setPassword(newPassword);
            System.out.println("[PASSWORD HAS BEEN CHANGED]");
        } else {
            System.out.println("[WRONG COMMAND]");
        }
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected user";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
