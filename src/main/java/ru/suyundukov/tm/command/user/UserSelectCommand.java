package ru.suyundukov.tm.command.user;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;

import java.util.Comparator;
import java.util.List;
@Component
public class UserSelectCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[SELECT USER]");
        if (!userService.getUserOnline().getRoleType().equals(RoleType.ADMIN)) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        List<User> userList = userService.findAll();
        userList.sort(Comparator.comparing(User::getName));
        for (int i = 0; i < userList.size(); i++) {
            System.out.println((i + 1) + ". " + userList.get(i).getName());
        }
    }

    @Override
    public String getName() {
        return "user-select";
    }

    @Override
    public String getDescription() {
        return "Select user";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}