package ru.suyundukov.tm.command.user;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.util.List;
@Component
public class UserSignInCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[USER SIGN IN]");
        if (userService.getUserOnline() != null) {
            System.out.println("[YOU ALREADY SIGNED UP , CURRENT USER : " + userService.getUserOnline().getName());
            return;
        }
        System.out.println("[ENTER LOGIN:]");
        String login = ConsoleHelper.readString();
        System.out.println("[ENTER PASSWORD:]");
        String password = ConsoleHelper.readString();
        String passwordMD5 = ConsoleHelper.md5Password(password);
        List<User> users = userService.findAll();
        if (!users.isEmpty()) {
            for (User user : users) {
                if ((user.getLogin().equals(login)) && (user.getPassword().equals(passwordMD5))) {
                    userService.setUserOnline(user);
                    System.out.println("[YOU ARE LOGGED IN]");
                    return;
                }
            }
            System.out.println("[USER HAS NOT FOUNDED OR PASSWORD IS INCORRECT]");
        } else {
            System.out.println("[THERE ARE NO OTHER USERS]");
        }
    }

    @Override
    public String getName() {
        return "user-signin";
    }

    @Override
    public String getDescription() {
        return "User login";
    }

    @Override
    public boolean isNeedAuthorization() {
        return false;
    }
}
