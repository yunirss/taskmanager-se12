package ru.suyundukov.tm.command.user;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.utils.ConsoleHelper;
@Component
public class UserSignOutCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER <<YES>> TO SIGN OUT]");
        String answer = ConsoleHelper.readString();
        if (answer.equalsIgnoreCase("yes")) {
            userService.setUserOnline(null);
            System.out.println("[YOU ARE SIGNED OUT]");
        } else {
            System.out.println("[OPERATION CANCELLED]");
        }
    }

    @Override
    public String getName() {
        return "user-signout";
    }

    @Override
    public String getDescription() {
        return "End of user session";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
