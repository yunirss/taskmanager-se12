package ru.suyundukov.tm.command;

import org.springframework.beans.factory.annotation.Autowired;
import ru.suyundukov.tm.service.*;

public abstract class AbstractCommand {
    @Autowired
    protected ProjectServiceInterface projectService;
    @Autowired
    protected TaskServiceInterface taskService;
    @Autowired
    protected UserServiceInterface userService;

    public abstract void execute() throws Exception;

    public abstract String getName();

    public abstract String getDescription();

    public abstract boolean isNeedAuthorization();

    @Override
    public String toString() {
        return getName() + " : " + getDescription();
    }
}
