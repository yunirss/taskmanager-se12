package ru.suyundukov.tm.command.task;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.enums.Status;
import ru.suyundukov.tm.utils.ConsoleHelper;

@Component
public class TaskEditCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK EDIT] :");
        System.out.println("[ENTER NAME OF TASK TO EDIT]");
        String taskName = ConsoleHelper.readString();
        if (taskService.findByName(taskName) != null) {
            final Task task = taskService.findByName(taskName);
            final User userOnline = userService.getUserOnline();
            if (task.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
                System.out.println("[ENTER NEW DESCRIPTION]");
                task.setDescription(ConsoleHelper.readString());
                System.out.println("[NEW DESCRIPTION ADDED]");
                System.out.println("[ENTER NEW STATUS : open, inprogress OR resolved]");
                String status = ConsoleHelper.readString().trim();
                while (true) {
                    if (status.equalsIgnoreCase(Status.OPEN.getName())) {
                        task.setStatus(Status.OPEN);
                        System.out.println("[NEW STATUS ADDED]");
                        break;
                    } else if (status.equalsIgnoreCase(Status.INPROGRESS.getName())) {
                        task.setStatus(Status.INPROGRESS);
                        System.out.println("[NEW STATUS ADDED]");
                        break;
                    } else if (status.equalsIgnoreCase(Status.RESOLVED.getName())) {
                        task.setStatus(Status.RESOLVED);
                        System.out.println("[NEW STATUS ADDED]");
                        break;
                    } else {
                        System.out.println("[WRONG COMMAND]");
                    }
                }
                taskService.save(task);
            } else {
                System.out.println("[ACCESS DENIED]");
            }
        } else {
            System.out.println("[NO SUCH TASK EXIST]");
        }
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
