package ru.suyundukov.tm.command.task;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;
@Component
public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK DELETE]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = ConsoleHelper.readString();
        if (taskService.findByName(taskName) != null) {
            final Task task = taskService.findByName(taskName);
            final User userOnline = userService.getUserOnline();
            if (task.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
                System.out.println("[DELETE TASK " + "<<" + taskName + ">>" + " ? YES or NO]");
                String name = ConsoleHelper.readString();
                if (name.equalsIgnoreCase("yes")) {
                    taskService.deleteById(task.getId());
                    System.out.println("[SELECTED TASK DELETED]");
                } else {
                    System.out.println("[NOT DELETED]");
                }
                return;
            }
            System.out.println("[ACCESS DENIED]");
        } else {
            System.out.println("[NO SUCH PROJECT EXIST]");
        }
    }

    @Override
    public String getName() {
        return "task-delete";
    }

    @Override
    public String getDescription() {
        return "Delete selected task.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
