package ru.suyundukov.tm.command.task;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;
import ru.suyundukov.tm.utils.DateHelper;
@Component
public class TaskSelectCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER TASK NAME] : ");
        String taskName = ConsoleHelper.readString();
        if (taskService.findByName(taskName) != null) {
            final Task task = taskService.findByName(taskName);
            final User userOnline = userService.getUserOnline();
            if (task.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
                System.out.println("[TASK ID] - " + task.getId());
                System.out.println("[TASK NAME] - " + task.getName());
                System.out.println("[TASK DESCRIPTION] - " + task.getDescription());
                System.out.println("[TASK STATUS] - " + task.getStatus());
                System.out.println("[PROJECT NAME] - " + projectService.findById(task.getProjectId()).getName());
                System.out.println("[CREATION DATE] - " + DateHelper.dateFormat(task.getCreateDate()));
                System.out.println("[CREATED BY] - " + userService.findById(task.getUserId()).getName());
            } else {
                System.out.println("[ACCESS DENIED]");
            }
        } else {
            System.out.println("[NO SUCH TASK EXIST]");
        }
    }

    @Override
    public String getName() {
        return "task-select";
    }

    @Override
    public String getDescription() {
        return "Select task.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
