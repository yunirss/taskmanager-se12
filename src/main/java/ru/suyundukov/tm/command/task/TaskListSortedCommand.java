package ru.suyundukov.tm.command.task;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;
import ru.suyundukov.tm.utils.DateHelper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
@Component
public class TaskListSortedCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[SORTING]");
        System.out.println("[WHAT TO DO? ENTER NUMBER]");
        System.out.println("1. SORT BY STATUS");
        System.out.println("2. SORT BY CREATE DATE");
        System.out.println("3. FIND BY UNQUALIFIED NAME");
        int answer = Integer.parseInt(ConsoleHelper.readString().trim());
        List<Task> taskList = taskService.findAll();
        final User userOnline = userService.getUserOnline();
        if (answer == 1) {
            System.out.println("[TASK SORTED LIST BY STATUS]");
            if (!userOnline.getRoleType().equals(RoleType.ADMIN)) {
                List<Task> taskValid = taskList.stream()
                        .filter(x -> userOnline.getId() == x.getUserId())
                        .sorted(Comparator.comparing(Task::getStatus))
                        .collect(Collectors.toList());
                for (int i = 0; i < taskValid.size(); i++) {
                    System.out.println((i + 1) + ". " + taskValid.get(i).getName() + " - " + taskValid.get(i).getStatus());
                }
            } else {
                taskList.sort(Comparator.comparing(Task::getStatus));
                for (int i = 0; i < taskList.size(); i++) {
                    System.out.println((i + 1) + ". " + taskList.get(i).getName() + " - " + taskList.get(i).getStatus());
                }
            }
        } else if (answer == 2) {
            System.out.println("[TASK SORTED LIST BY CREATE DATE]");
            if (!userOnline.getRoleType().equals(RoleType.ADMIN)) {
                List<Task> taskValid = taskList.stream()
                        .filter(x -> userOnline.getId() == x.getUserId())
                        .sorted(Comparator.comparing(Task::getCreateDate))
                        .collect(Collectors.toList());
                for (int i = 0; i < taskValid.size(); i++) {
                    System.out.println((i + 1) + ". " + taskValid.get(i).getName() + " - " + DateHelper.dateFormat(taskValid.get(i).getCreateDate()));
                }
            } else {
                taskList.sort(Comparator.comparing(Task::getCreateDate));
                for (int i = 0; i < taskList.size(); i++) {
                    System.out.println((i + 1) + ". " + taskList.get(i).getName() + " - " + DateHelper.dateFormat(taskList.get(i).getCreateDate()));
                }
            }
        } else if (answer == 3) {
            System.out.println("[FIND BY UNQUALIFIED NAME]");
            System.out.println("[ENTER NAME]");
            String name = ConsoleHelper.readString().trim();
            if (name.length() >= 3) {
                List<Task> taskTmp = new ArrayList<>();
                List<Task> tasks = taskList.stream()
                        .filter(x -> userOnline.getId() == x.getUserId())
                        .sorted(Comparator.comparing(Task::getName))
                        .collect(Collectors.toList());
                for (int i = 0; i < tasks.size(); i++) {
                    if (tasks.get(i).getName().startsWith(name)) {
                        System.out.println((i + 1) + ". " + tasks.get(i).getName());
                        taskTmp.add(tasks.get(i));
                    }
                }
                if (taskTmp.isEmpty()) {
                    System.out.println("[TASKS WITH THIS NAME NOT FOUND]");
                }
            } else {
                System.out.println("[ENTER AT LEAST 3 LETTERS]");
            }
        } else {
            System.out.println("[TRY AGAIN]");
        }
    }

    @Override
    public String getName() {
        return "task-list-sorted";
    }

    @Override
    public String getDescription() {
        return "Show all sorted tasks";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
