package ru.suyundukov.tm.command.task;

import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.enums.Status;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.time.LocalDateTime;

@Component
public class TaskCreateCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATED]");
        System.out.println("[ENTER NAME:]");
        String name = ConsoleHelper.readString();
        System.out.println("[ENTER DESCRIPTION:]");
        String description = ConsoleHelper.readString();
        System.out.println("[ENTER NAME OF PROJECT:]");
        String projectName = ConsoleHelper.readString();
        if (projectService.findByName(projectName) != null) {
            final Project project = projectService.findByName(projectName);
            final Task task = new Task();
            task.setName(name);
            task.setDescription(description);
            task.setCreateDate(LocalDateTime.now());
            task.setStatus(Status.OPEN);
            task.setUserId(userService.getUserOnline().getId());
            task.setProjectId(project.getId());
            taskService.save(task);
            System.out.println("[OK]");
        } else {
            System.out.println("NO SUCH PROJECT EXISTS");
        }
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;

    }
}