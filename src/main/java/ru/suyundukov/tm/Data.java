package ru.suyundukov.tm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Data implements Serializable {
    List<Project> projects = new ArrayList<>();
    List<Task> tasks = new ArrayList<>();
    List<User> users = new ArrayList<>();
}
