package ru.suyundukov.tm.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Утилитный класс служит для форматирования даты
 */
public class DateHelper {
    private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    public static String dateFormat(LocalDateTime localDateTime) {
        return dateTimeFormatter.format(localDateTime);
    }
}
