package ru.suyundukov.tm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Утилитный класс, для работы с сущностью проекта
 */
public class ConsoleHelper {
    private static final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static String readString() {
        String string;
        try {
            string = bufferedReader.readLine();
        } catch (IOException e) {
            System.out.println("[ERROR, TRY AGAIN]");
            string = readString();
        }
        return string;
    }

    public static String md5Password(String password) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] bytes = md5.digest(password.getBytes());
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02X", b));
        }
        return builder.toString();
    }
}
