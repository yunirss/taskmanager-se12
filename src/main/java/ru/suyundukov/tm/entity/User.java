package ru.suyundukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.suyundukov.tm.enums.RoleType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "login", unique = true, nullable = false)
    private String login;
    @Column(name = "password", nullable = false)
    private String password;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.")
    @Column(name = "createdate", nullable = false)
    private LocalDateTime createDate;
    @Column(name = "roletype", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    public User(Integer id, String name, String login, String password, LocalDateTime createDate, RoleType roleType) {
        this.login = login;
        this.id = id;
        this.name = name;
        this.createDate = createDate;
        this.password = password;
        this.roleType = roleType;
    }

    public User() {

    }
}