package ru.suyundukov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.suyundukov.tm.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {
    Task findTaskById(Integer id);

    Task findTaskByName(String name);

    void deleteTaskByName(String name);

    void deleteTaskById(Integer id);

    void deleteTasksByProjectId(Integer id);

    List<Task> findTasksByProjectId(int projectId);

    List<Task> findTasksByUserId(int userId);
}
