package ru.suyundukov.tm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.command.defaultCommands.ExitCommand;
import ru.suyundukov.tm.service.UserService;
import ru.suyundukov.tm.service.UserServiceInterface;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.util.List;

@Component
public class Bootstrap {
    @Autowired
    public List<AbstractCommand> commands;
    @Autowired
    private UserServiceInterface userService;

    public void start() {
        System.out.println("WELCOME TO TASK MANAGER");
        while (true) {
            try {
                String string = ConsoleHelper.readString().trim();
                AbstractCommand command = null;
                for (AbstractCommand abstractCommand : commands) {
                    if (abstractCommand.getName().equalsIgnoreCase(string)) {
                        if (userService.getUserOnline() == null) {
                            if (abstractCommand.isNeedAuthorization()) {
                                System.out.println("[YOU NEED LOG IN TO USE THIS COMMANDS]");
                                break;
                            }
                        }
                        command = abstractCommand;
                        command.execute();
                        break;
                    }
                }
                if (command == null) {
                    System.out.println("[PLEASE, TYPE CORRECT COMMAND OR ENTER <<help>>]");
                } else if(command instanceof ExitCommand) {
                    break;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}