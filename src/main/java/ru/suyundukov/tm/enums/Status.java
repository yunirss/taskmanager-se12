package ru.suyundukov.tm.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Status {
    OPEN("open"),
    INPROGRESS("inprogress"),
    RESOLVED("resolved");

    private final String name;
}
