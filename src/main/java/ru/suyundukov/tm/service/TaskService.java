package ru.suyundukov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService implements TaskServiceInterface {
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    public void save(Task task) {
        taskRepository.save(task);
    }

    @Override
    public List<Task> findAll() throws Exception {
        return taskRepository.findAll();

    }

    @Override
    public Task findById(Integer id){
        return taskRepository.findTaskById(id);
    }

    @Override
    public Task findByName(String name) throws Exception {
        return taskRepository.findTaskByName(name);
    }

    public List<Task> findTasksByProjectId(int projectId){
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public List<Task> findTasksByUserId(int userId) {
        return taskRepository.findTasksByUserId(userId);
    }
    @Transactional
    public void deleteTasksByProjectId(int id) {
        taskRepository.deleteTasksByProjectId(id);
    }

    @Override
    @Transactional
    public void deleteByName(String name) {
        taskRepository.deleteTaskByName(name);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        taskRepository.deleteTaskById(id);
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(Task[] tasks) {
        for (Task task : tasks) {
            save(task);
        }
    }

    @Override
    public void load(List<Task> tasks) {
        for (Task task : tasks) {
            save(task);
        }
    }
}