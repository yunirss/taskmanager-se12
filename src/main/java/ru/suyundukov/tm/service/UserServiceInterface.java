package ru.suyundukov.tm.service;

import ru.suyundukov.tm.entity.User;

public interface UserServiceInterface extends AbstractService<User> {
    User getUserOnline();
    void setUserOnline(User userOnline);
}
