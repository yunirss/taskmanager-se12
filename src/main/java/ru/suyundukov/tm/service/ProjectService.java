package ru.suyundukov.tm.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.repository.ProjectRepository;

import java.util.List;

@AllArgsConstructor
@Service
public class ProjectService implements ProjectServiceInterface{
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Transactional
    public void save(Project project) {
        projectRepository.save(project);
    }

    @Override
    public List<Project> findAll() throws Exception {
        return projectRepository.findAll();
    }

    @Override
    public Project findById(Integer id){
        return projectRepository.findProjectById(id);
    }

    @Override
    public Project findByName(String name) throws Exception {
        return projectRepository.findProjectByName(name);
    }

    @Transactional
    public void deleteByName(String name) throws Exception {
        projectRepository.deleteProjectByName(name);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        projectRepository.deleteProjectById(id);
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(Project[] projects) {
        for (Project project : projects) {
            save(project);
        }
    }

    @Override
    public void load(List<Project> projectList) {
        for (Project project : projectList) {
            save(project);
        }
    }
}
