package ru.suyundukov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.repository.UserRepository;

import java.util.List;
@Service
public class UserService implements UserServiceInterface {
    @Autowired
    private UserRepository userRepository;
    private User userOnline;

    public User getUserOnline() {
        return userOnline;
    }

    public void setUserOnline(User userOnline) {
        this.userOnline = userOnline;
    }

    @Override
    @Transactional
    public void save(User user){
        userRepository.save(user);
    }

    @Override
    public User findById(Integer id){
        return userRepository.findUserById(id);
    }

    @Override
    public User findByName(String name) throws Exception {
        return userRepository.findUserByName(name);
    }

    @Override
    public List<User> findAll() throws Exception {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        userRepository.deleteUserById(id);
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(User[] users){
        for (User user : users) {
            save(user);
        }
    }

    @Override
    public void load(List<User> users){
        for (User user : users) {
            save(user);
        }
    }
}