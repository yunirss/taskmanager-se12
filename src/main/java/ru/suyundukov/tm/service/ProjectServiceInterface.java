package ru.suyundukov.tm.service;

import ru.suyundukov.tm.entity.Project;

public interface ProjectServiceInterface extends AbstractService<Project> {
    void deleteByName(String name) throws Exception;
}
