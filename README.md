**Task Manager**

`Описание проекта`

> TASK MANAGER - консольное приложение, в котором можно создавать свои проекты и задачи зарегистрировавшись под обычным пользователем или администратором. Во время регистрации ваш пароль будет закодирован по MD5 алгоритму. В каждый проект можно добавлять описание, а так же его редактировать. Каждую задачу можно создавать внутри созданного проекта, так же добавлять задачу в проект. При просмотре проекта или задачи - будет выведено название, описание, время и дата создания и изменения (если был изменен).Реализована сортировка, а также статус проектов и задач. Добавлена сериализация (импорт и экспорт) данных в форматы .json, .xml и .bin по выбору пользователя.Хранения данных осуществляется в СУБД PostgreSQL.Репозитории реализованы на основе технологии Spring Data JPA.

`Требования к SOFTWARE`
- MacOs
- Windows
- Linux

`Стек технологий`
- IntelliJ IDEA Ultimate
- Java SE Development 8
- Apache Maven version 4.0.0
- Jackson (Serialization)
- MVC(repository, service, ServiceLocator)
- Git
- MD5
- PostgreSQL
- Hibernate (JDBC)
- Spring Context
- Spring Data JPA.

`Разработчик`
- Суюндуков Юнир Ильфатович
- yunir14@yandex.ru

`Консольные команды:`
- export: Export data.
- import: Import data.
- help: Show all commands.
- project-select: Select project by name.
- project-edit: Edit selected project.
- project-create: Create new project.
- project-list: Show all projects.
- project-delete: delete project.
- task-select: Select project by name.
- task-edit: Edit selected project.
- task-create: Create new task.
- task-list: Show all tasks.
- task-delete: delete task.
- user-signup: New user registration.
- user-signin: Sign in to program with login and password.
- user-signout: User sign out from program.
- user-select: Admin use only. Select user and get info.
- user-edit: Edit user's name or password.
- exit: Exit the Task manager


